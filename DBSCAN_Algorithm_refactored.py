# -*- coding: utf-8 -*-
"""
Created on Thu Jun  9 14:33:51 2022

@author: Walbersy
"""

import pandas as pd
import numpy as np
from sklearn.cluster import DBSCAN
#from collections import Counter
from sklearn.preprocessing import StandardScaler
from pylab import rcParams
rcParams['figure.figsize'] = 14,6
#import matplotlib.pyplot as plt
from sklearn.neighbors import NearestNeighbors
import ruptures as rpt 
import geopy.distance
import requests


#data = pd.read_csv('C:/Users/Walbersy/Desktop/Documentos/Practica/IntentHQ/Datasets/Route15sec.csv')
#data = pd.read_csv('C:/Users/Walbersy/Desktop/Documentos/Practica/IntentHQ/Datasets/Bike_Activity5sec.csv')
data = pd.read_csv('C:/Users/Walbersy/Desktop/Documentos/Practica/IntentHQ/Datasets/dummy_10secs.csv')
#data = data.loc[13:79]


#if 30 secs, n_bkps = 5
#if 15 secs, n_bkps = 7
#if 5 secs, n_bkps = 15


data["Time"]=pd.to_datetime(data.Time)

def n_bkps_definition(data):
    """
    n_bkps_definition() function defines according to the time in seconds between points (30 sec, 20 sec, ..., n) the
    value of the n_bkps which be use to cut rupture the elbow graph.

    Parameters
    ----------
    data : DataFrame: Raw data with the next fields: ['Time', 'Lon', 'Lat']

    Returns
    -------
    n_bkps_value : Single int number

    """
    average_time_between_points = np.empty((len(data),1))
    for i in range(1,len(data)):
        average_time_between_points[i-1,0] = (data.iloc[i-1,0]-data.iloc[i,0]).total_seconds()*(-1)
        
    average_time_between_points=pd.DataFrame(average_time_between_points)
    average_time_between_points.columns = ['Seconds']  
    average_time_between_points = average_time_between_points.head(average_time_between_points.shape[0] -1)
    mode=average_time_between_points.Seconds.mode()
    if (mode[0] >= 20):
        n_bkps_value = 5
    elif(mode[0] < 20 and mode[0]>= 15): 
        n_bkps_value = 7    
    else:
        n_bkps_value = 15     
    return n_bkps_value

n_bkps_value=n_bkps_definition(data)



def pairwise_distance_calculation(data):
    """
    outlier_detection() returns de distances that are considered outliers. 
    It uses the distances between every pairwaise consecutive points in order to analyze eventually the presence of outliers.

    Parameters
    ----------
    data : DataFrame: Raw data with the next fields: ['Time', 'Lon', 'Lat']

    Returns
    -------
    outliers : DataFrame: Return dataframe with the next fields: ['Points', 'Distance', 'Outlier']

    """
    #This chunk will estimate the distances in meters between every consecutive pairwise of points.
    distance = np.empty((len(data),1))
    for i in range(1,len(data)):
        distance[i-1, 0]=geopy.distance.geodesic((data.iloc[i-1,1], data.iloc[i-1,2]), (data.iloc[i,1], data.iloc[i,2])).m
    distance=pd.DataFrame(distance)
    distance.columns = ['Distance']   
    distance = distance.head(distance.shape[0] -1)
    
    #This chuck just sort the pairwise of points with its correspondent distance. 
    consecutive_points = np.empty((len(data),1))
    consecutive_points=consecutive_points.astype(str)
    for i in range(1,len(data)):
        consecutive_points[i-1, 0]=data.index.astype(str)[i-1] + '-' + data.index.astype(str)[i]
    consecutive_points=pd.DataFrame(consecutive_points)
    consecutive_points.columns = ['Points']   
    consecutive_points = consecutive_points.head(consecutive_points.shape[0] -1)
    distance=pd.concat([consecutive_points, distance], axis = 1)
    
    #This chunk identify the potential outliers
    #distance['Outlier']=np.where(distance['Distance'] > 1600, 'Outlier', '')
    #outliers=distance
    #return outliers
    return distance

distance = pairwise_distance_calculation(data)



def consecutive_points(data):
    """
    consecutive_points() function create a consecutive Id for every waypoint created.

    Parameters
    ----------
    data : DataFrame: Raw data with the next fields: ['Time', 'Lon', 'Lat']

    Returns
    -------
    data : DataFrame: Raw data with the next fields: ['Points', 'Time', 'Lon', 'Lat']

    """

    data=pd.concat([pd.DataFrame(np.array(range(1,len(data.index)+1))), data], axis = 1)
    data.rename(columns={0:'PointId'}, inplace=True)
    return data
data = consecutive_points(data)



def coordinates_data_preparation(data):
    """
    coordinates_data_preparation() extract the coordinates from the original data, and it prepare
    for the next step that is the distances calculation

    Parameters
    ----------
    data : DataFrame: Raw data with the next fields: ['Points', 'Time', 'Lon', 'Lat']

    Returns
    -------
    dbscan_data : Array with two columns ('Lat' and 'Lon')

    """
    
    dbscan_data = data[['Lon', 'Lat']]
    dbscan_data = dbscan_data.values.astype('float32', copy = False)
    dbscan_data_scaler = StandardScaler().fit(dbscan_data)
    dbscan_data = dbscan_data_scaler.transform(dbscan_data)
    return dbscan_data
dbscan_data=coordinates_data_preparation(data)



def distances_estimation(dbscan_data):
    """
    distances_estimation() function estimates the distances of the points using the 5 nearest neighbors, in order
    to determine the existence af proximity between points. Returns an array with the distances ordered ascending.

    Parameters
    ----------
    dbscan_data : Array with two columns ('Lat' and 'Lon')

    Returns
    -------
    distances : Array with one field with the distances sorted ascending
    """
    
    neigh = NearestNeighbors(n_neighbors=5)
    nbrs = neigh.fit(dbscan_data)
    distances, indices = nbrs.kneighbors(dbscan_data)
    distances = np.sort(distances, axis = 0)
    distances = distances[:,1]
    return distances
distances = distances_estimation(dbscan_data)



def dbscan_epsilon_estimation(distances,distance):
    """
    dbscan_epsilon_estimation() function estimates the proper epsilon value for running the dbscan algorithm in the next function.

    Parameters
    ----------
    distances : Array with one field with the distances sorted ascending
    outliers : DataFrame: Return dataframe with the next fields: ['Points', 'Distance', 'Outlier']

    Returns
    -------
    threshold : Single float that represent the epsilon for the dbscan algorithm.
    """
    s=len(distances)
    n_samples, n_dims, sigma = s, 1, 1
    n_bkps = n_bkps_value
    signal, bkps = rpt.pw_constant(n_samples, n_dims, n_bkps, noise_std=sigma)
    algo = rpt.Dynp(model="l2").fit(distances)
    result = algo.predict(n_bkps=n_bkps_value)
    rpt.display(distances, bkps, result)
    h = pd.DataFrame(distances)
    eps = h.iloc[0:result[0]+1][0]
    eps=pd.DataFrame(eps)
    eps.columns = ['V1']
    eps=eps.loc[(eps!=0).any(axis=1)]
    if (distance['Distance'].min() > 5):
        threshold = eps["V1"].min()
    else:
        threshold = eps["V1"].max()
    return threshold
threshold=dbscan_epsilon_estimation(distances,distance)



def dbscan_model_run(threshold, dbscan_data):
    """
    dbscan_model_run() funtion run the dbscan algorithm identinfying which points belongs to cluster and those not.
    
    Parameters
    ----------
    threshold : Single float that represent the epsilon for the dbscan algorithm.
    dbscan_data : Array with two columns ('Lat' and 'Lon')

    Returns
    -------
    outliers_df : Data Frame with the points that do not belong to a cluster. ['PointId', 'Tiempo', 'Lon', 'Lat']
    cluster_df : Data Frame with the points that creates o belong to a cluster. ['PointId', 'Tiempo', 'Lon', 'Lat']

    """
    model = DBSCAN(eps = threshold, min_samples=4, metric = 'haversine').fit(dbscan_data)
    outliers_df = data[model.labels_ == -1]
    cluster_df = data[model.labels_ != -1]
    return outliers_df, cluster_df
outliers_df, cluster_df = dbscan_model_run(threshold, dbscan_data)


      
def places_of_interest(cluster_df):
    """
    places_of_interest() function creates the final Data Frame with the points of interest

    Parameters
    ----------
    cluster_df : Data Frame with the points that creates o belong to a cluster. Fields: ['PointId', 'Tiempo', 'Lon', 'Lat']

    Returns
    -------
    points_stay : Data Frame with the points of interest. Fields: ['PointInitial', 'PointEnding', 'TimeInitial', 'TimeEnding', 'Lon', 'Lat']
    """
    if (len(cluster_df)>0):
        consecutive_cluster = np.empty((len(cluster_df),1)) 
        consecutive_cluster=(np.diff(cluster_df.PointId) != 1).cumsum()
        consecutive_cluster = pd.DataFrame(consecutive_cluster)
        consecutive_cluster.columns = ['IdCluster']
        a = consecutive_cluster['IdCluster'].iloc[0]

        if (cluster_df['PointId'].iloc[1] - cluster_df['PointId'].iloc[0] == 1):
            consecutive_cluster.loc[-1] = [a]
            consecutive_cluster.index = consecutive_cluster.index+1
            consecutive_cluster.sort_index(inplace = True)
        else:
            consecutive_cluster.loc[-1] = [a-1]
            consecutive_cluster.index = consecutive_cluster.index+1
            consecutive_cluster.sort_index(inplace = True)

        cluster_df = pd.concat([cluster_df.reset_index(drop=True), pd.DataFrame(consecutive_cluster)], axis=1,ignore_index=True)
        cluster_df.columns = ['Points', 'Time', 'Lon', 'Lat','IdCluster']
        cluster_df = cluster_df.groupby(['IdCluster']).filter(lambda x: len(x)>1).reset_index(drop=True)

        points_stay=cluster_df.groupby('IdCluster').aggregate({'Points':['min','max'],'Time':['min','max'],'Lat':'mean','Lon':'mean'})
        points_stay.columns = ['PointInitial', 'PointEnding', 'TimeInitial', 'TimeEnding', 'Lat', 'Lon']
        points_stay["PointsId"] = points_stay["PointInitial"].astype(str) + '-' + points_stay["PointEnding"].astype(str)
        points_stay.drop(['PointInitial', 'PointEnding'], axis = 1, inplace = True)
        points_stay["TotalTime"] = points_stay.TimeEnding-points_stay.TimeInitial
        points_stay["TotalTime"] = points_stay["TotalTime"] / np.timedelta64(1, 's')
        points_stay=points_stay[['PointsId', 'TimeInitial', 'TimeEnding', 'Lat', 'Lon', 'TotalTime']]

    else:
        
        points_stay = pd.DataFrame(columns=['PointsId', 'TimeInitial', 'TimeEnding', 'Lat', 'Lon', 'TotalTime'])
    return points_stay

points_stay = places_of_interest(cluster_df)


if (len(points_stay)>0):
    
    def POI(points_stay):
        points_stay["Category"]=""
        points_stay["Type"]=""
        points_stay["Name"]=""
        points_stay=points_stay. reset_index() 
        points_stay.drop('IdCluster', axis=1, inplace=True)

        for i in range(1,len(points_stay)+1):
            lat=points_stay.iloc[i-1,3].astype(str)
            lon=points_stay.iloc[i-1,4].astype(str)
            url = 'https://nominatim.openstreetmap.org/reverse?format=geojson&lat={lat}&lon={lon}'.format(lat=lat, lon=lon)
            result = requests.get(url=url)
            result_json = result.json()
            features=result_json['features']
            for res3 in features[:1]:
               res4 = res3['properties']
               points_stay.at[i-1,"Category"] = res4['addresstype']
               points_stay.at[i-1,"Type"] = res4['type']
               points_stay.at[i-1,"Name"] = res4['name']
        return points_stay
    points_stay = POI(points_stay)






       
       
       



           
























#Bibliography

#file:///C:/Users/Walbersy/Downloads/SEGAbstract-StressFlow.pdf